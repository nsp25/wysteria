/*
 * generates median code for the Kerschbaum example for n elements
 * n is taken as input on command line
 * to compile: g++ median.cpp
 * to run: ./a.out
 * known to be working with gcc 4.7 on mac osx 10.8.2
 */

#include<fstream>
#include<iostream>

#include<cmath>
#include<list>
#include<string>
#include<sstream>
#include<stdlib.h>

using namespace std;

int n = 0;
//int alice[] = { 1, 3, 4, 7, 9, 13, 14, 16 };
//int bob[] = { 2, 5, 6, 8, 10, 11, 12, 15 };

ofstream outfile;

string getBoolName()
{
    static int n = 1;
    
    stringstream ss;
    ss << "b" << n++;
    return ss.str();
}

string getTmpName()
{
    static int n = 1;
    
    stringstream ss;
    ss << "t" << n++;
    return ss.str();
}

string getFieldName(int n)
{
    stringstream ss;
    ss << "#f" << n;
    return ss.str();
}

void printHeader(int n)
{
    int alice_index = 1;
    int bob_index = 1;

    for(int i = 1; i <= n; ++i) {
	outfile << "let alice" << i << " = wire !Alice:" << i << " in" << endl;
    }

    for(int i = 1; i <= n; ++i) {
	outfile << "let bob" << i << " = wire !Bob:" << (i + n) << " in" << endl;
    }

    // outfile << "let w1 = wire !Alice:{";
    // for(int i= 1; i <= n; ++i) {
    // 	outfile << getFieldName(i) << " : " << i;
    // 	if(i != n) { outfile << ", "; }
    // }
    // outfile << "} in\n";

    // outfile << "let w2 = wire !Bob:{";
    // for(int i= 1; i <= n; ++i) {
    // 	outfile << getFieldName(i) << " : " << (i + n);
    // 	if(i != n) { outfile << ", "; }
    // }
    // outfile << "} in\n\n";    
}

string getVName(int n, bool alice)
{
    stringstream ss;
    ss << (alice ? "alice" : "bob") << n << (alice ? "[!Alice]" : "[!Bob]");
    return ss.str();
}

void printTabs(int n)
{
    for(int i = 1; i <= n; ++i) { outfile << " "; }
}

void printBody(int xmin, int xmax, int ymin, int ymax, int tabs)
{
    int xmid = (xmin + xmax) / 2;
    int ymid = (ymin + ymax) / 2;
    int xnewmin, xnewmax, ynewmin, ynewmax;
    
    string aname = getVName(xmid, true);
    string bname = getVName(ymid, false);

    string boolname = getBoolName();
    outfile << "let " << boolname << " @ sec({!Alice, !Bob}) = " << aname << " > " << bname << " in\n";

    outfile << "match " << boolname << " with " << "\n"
	 << "Left " << getTmpName() << " => ";

    int newtabs = tabs + 2;
    /* both parties have one element each left */
    if(xmin == xmax && ymin == ymax) {
	//outfile << aname << endl;	
	//outfile << boolname << endl;
	string mname1 = getTmpName ();
	outfile << "let " << mname1 << " @ sec({!Alice, !Bob}) = "
		<< "let " << getTmpName() << " = " << aname << " in" << endl
		<< "let " << getTmpName() << " = " << bname << " in" << endl
		<< aname << endl << " in " << mname1 << endl;
	outfile << "| Right " << getTmpName() << " => ";
	//outfile << bname << endl;
	string mname2 = getTmpName ();
	outfile << "let " << mname2 << " @ sec({!Alice, !Bob}) = "
		<< "let " << getTmpName() << " = " << aname << " in" << endl
		<< "let " << getTmpName() << " = " << bname << " in" << endl
		<< bname << endl << " in " << mname2 << endl;
	//outfile << boolname << endl;
	outfile << "mend\n";
	return;
    } else {
	/*
	 * select ceil(n / 2) elements for each party (lower or top, depending on result
	 * of comparison), to go to next round
	 */
	int c = ceil((((double) xmax) - ((double) xmin) + 1)/((double) 2));

	/* for then branch of a < b, select top half of Alice, lower half of Bob */
	xnewmax = xmax; xnewmin = xmax - c + 1; ynewmin = ymin; ynewmax = ymin + c - 1;
	printBody(xnewmin, xnewmax, ynewmin, ynewmax, tabs + 2);

	outfile << endl;
	outfile << "| Right " << getTmpName() << " => ";
	/* for else branch of a < b, select lower half of Alice, top half of Bob */
	xnewmin = xmin; xnewmax = xmin + c - 1; ynewmax = ymax; ynewmin = ymax -c + 1;
	printBody(xnewmin, xnewmax, ynewmin, ynewmax, tabs + 2);
	
	outfile << endl;
	outfile << "mend\n";
    }
}

int main(int argc, char **argv)
{
    n = atoi(argv[1]);

    outfile.open("optmed.wy", ios::out | ios::trunc);
    printHeader(n);   

    outfile << "let m = " << endl;

    printBody(1, n, 1, n, 2);

    outfile << "\n\nin\n\nm" << endl;
    outfile << endl;

    outfile.close();
    return 0;
}
