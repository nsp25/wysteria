#include<stdlib.h>
#include<iostream>

using namespace std;

int num_items = 16;
int range = 130;

int main()
{
    cout << "let cust = !Alice in" << endl;
    cout << "let isps = { !Bob, !Charlie } in" << endl;
    
    /* Customer's availability array */
    cout << "let avail @ par(cust) = " << endl;
    cout << "\t" << "let a = array [" << num_items  << "] of 0 in" << endl;
    for(int i = 0; i < num_items; ++i) {
	int r = rand() % 2;
	cout << "\t" << "let d" << i << " = update a[" << i << "] <- " << r << " in" << endl;
    }
    cout << "\t" << "a" << endl;
    cout << "in" << endl;
    
    /* Bob's bandwidth array */
    cout << "let bobbw @ par({!Bob}) = " << endl;
    cout << "\t" << "let abob = array [" << num_items << "] of 0 in" << endl;
    for(int i = 0; i < num_items; ++i) {
	if(i % 2 == 0) {
	    cout << "\t" << "let d" << i << " = update abob[" << i << "] <- "
		 << rand() % range << " in" << endl;
	} else {
	    cout << "\t" << "let d" << i << " = update abob[" << i << "] <- 0 in" << endl;
	}
    }
    cout << "\t" << "abob" << endl;
    cout << "in" << endl;
    
    /* charlie's bandwidth array */
    cout << "let charliebw @ par({!Charlie}) = " << endl;
    cout << "\t" << "let acharlie = array [" << num_items << "] of 0 in" << endl;
    for(int i = 0; i < num_items; ++i) {
	if(i % 2 == 0) {
	    cout << "\t" << "let d" << i << " = update acharlie[" << i << "] <- 0 in" << endl;
	} else {
	    cout << "\t" << "let d" << i << " = update acharlie[" << i << "] <- "
		 << rand() % range << " in" << endl;
	}
    }
    cout << "\t" << "acharlie" << endl;
    cout << "in" << endl;
    
    /* who has which array */
    cout << "let alloc = array [" << num_items << "] of cust in" << endl;
    for(int i = 0; i < num_items; ++i) {
	if(i % 2 == 0) {
	    cout << "let d" << i << " = update alloc[" << i << "] <- !Bob in" << endl;
	} else {
	    cout << "let d" << i << " = update alloc[" << i << "] <- !Charlie in" << endl;
	}
    }

    cout << "let wcust = wire !Alice:avail in" << endl;
    cout << "let w1 = wire !Bob:bobbw in" << endl;
    cout << "let w2 = wire !Charlie:charliebw in" << endl;
    cout << "let w3 = w1 ++ w2 in" << endl;
    cout << "((((f cust) wcust) isps) w3) alloc" << endl;
}
