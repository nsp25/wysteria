#include<vector>
#include<sstream>
#include<iostream>

using namespace std;


int n = 0;

int max_value = 0;

double fraction = 0.0;
vector<int> a_inp;
vector<int> b_inp;

void geninps()
{
    int i = 0;
    for(i = 0; i < n; ++i) {
	a_inp.push_back(rand() % max_value);
    }
    int common_elems = ((int) (n * fraction));
    
    for(i = 0; i < common_elems; ++i) {
	b_inp.push_back(a_inp[i]);
    }
    for(int j = i; j < n; ++j) {
	b_inp.push_back(rand() % max_value);
    }
}

string getBobVarName(int fld)
{
    static int count = 0;
    
    stringstream ss;
    ss << "t" << count++ << "_Bob_f" << fld;
    return ss.str();
}

string getAliceVarName(int fld)
{
    static int count = 0;
    
    stringstream ss;
    ss << "t" << count++ << "_Alice_f" << fld;
    return ss.str();
}

string getCmpName()
{
    static int count = 0;

    stringstream ss;
    ss << "c" << count++;
    return ss.str();
}

string getSumName()
{
    static int count = 0;
    
    stringstream ss;
    ss << "tmp" << count++;
    return ss.str();
}

void printHeader()
{
    cout << "let r1 @ par({!Alice}) = { ";
    for(int i = 1; i <= n; ++i) {
	cout << "#f" << i << " : " << (a_inp[i-1]);
	if(i < n) {
	    cout << ", ";
	} else {
	    cout << " ";
	}	
    }
    cout << "} in" << endl;
    cout << "let r2 @ par({!Bob}) = { ";
    for(int i = 1; i <= n; ++i) {
	cout << "#f" << i << " : " << (b_inp[i-1]);
	if(i < n) {
	    cout << ", ";
	} else {
	    cout << " ";
	}	
    }
    cout << "} in" << endl;

    cout << "let w1 = wire !Alice:r1 in" << endl;
    cout << "let w2 = wire !Bob:r2 in" << endl;
}

void print_Bob(string aliceVarName, int j)
{
    string vname = getBobVarName(j);
    string cname = getCmpName();
    cout << "let " << vname << " = r_Bob.#f" << j << " in" << endl;
    cout << "let " << cname << " = " << aliceVarName << " = " << vname << " in" << endl;
    cout << "if " << cname << " then true" << endl;
    cout << "else" << endl;
    /*cout << "match " << cname << " with " << endl;
      cout << "| Left " << getSumName() << " => " << endl;*/
    if(j == n) {
	cout << "false" << endl;
    } else {
	print_Bob(aliceVarName, j + 1);
    }
    /*cout << "| Right " << getSumName() << " => Right ()" << endl;
      cout << "mend" << endl;*/
}

void print_Alice(int i, vector<string>& cnames)
{
    if(i <= n) {
	string vname = getAliceVarName(i);
	string cname = getCmpName();
	cnames.push_back(cname);
	cout << "let " << vname << " = r_Alice.#f" << i << " in" << endl;    
	cout << "let " << cname << " = " << endl;
    
	print_Bob(vname, 1);
	cout << endl;
	cout << "in" << endl;
    
	print_Alice(i + 1, cnames);
    }
}

int main(int argc, char** argv)
{
    n = atoi(argv[1]);
    max_value = atoi(argv[2]);
    fraction = atof(argv[3]);
    geninps();

    printHeader();
    
    cout << "let r @ sec({!Alice, !Bob}) = " << endl;
    cout << "let r_Alice = w1[!Alice] in" << endl;
    cout << "let r_Bob = w1[!Alice] in" << endl;

    vector<string> cnames;
    print_Alice(1, cnames);
    
    cout << "let retr = {";
    for(int i = 0; i < n; ++i) {
	cout << "#f"<< (i + 1) << ":" << cnames[i];
	if(i == n - 1) {
	    cout << " ";
	} else {
	    cout << ", ";
	}
    }
    cout << "} in" << endl;
    cout << "retr" << endl;

    cout << "in\nr" << endl;        
}
