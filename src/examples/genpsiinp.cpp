#include<vector>
#include<sstream>
#include<stdlib.h>
#include<iostream>

using namespace std;

int n = 2;

int max_value = 100;

double fraction = 1;

vector<int> a_inp;
vector<int> b_inp;

void geninps()
{
    int i = 0;

    srand(7587540);

    for(i = 0; i < n; ++i) {
	a_inp.push_back(rand() % max_value);
    }
    int common_elems = ((int) (n * fraction));
    
    for(i = 0; i < common_elems; ++i) {
	b_inp.push_back(a_inp[i]);
    }
    for(int j = i; j < n; ++j) {
	b_inp.push_back(rand() % max_value);
    }
}

string getTmpVarName()
{
    static int i = 0;
    stringstream ss;

    ss << "t" << i++;
    return ss.str();
}

int main(int argc, char **argv)
{
    n = atoi(argv[1]);
    max_value = atoi(argv[2]);
    fraction = atof(argv[3]);
    geninps();
    
    cout << "let n = " << n << " in" << endl;
    
    cout << "let a @ par({!Alice}) = array [ n ] of 0 in" << endl;
    cout << "let b @ par({!Bob}) = array [ n ] of 0 in" << endl;
    
    for(int i = 0; i < n; ++i) {
	cout << "let " << getTmpVarName() << " @ par({!Alice}) = update a[" << i << "] <- " << a_inp[i] << " in" << endl;
	cout << "let " << getTmpVarName() << " @ par({!Bob}) = update b[" << i << "] <- " << b_inp[i] << " in" << endl;
    }
    
    return 0;
}
